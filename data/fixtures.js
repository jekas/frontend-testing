
export const catalogCardItem = [
	{
		title: 'Asus',
		url: '#',
		id: '1'
	},
	{
		title: 'Acer',
		url: '#',
		id: '2'
	},
	{
		title: 'HP (Hewlett Packcard)',
		url: '#',
		id: '3'
	},
	{
		title: 'Lenovo',
		url: '#',
		id: '4'
	},
	{
		title: 'Dell',
		url: '#',
		id: '5'
	},
	{
		title: 'Apple',
		url: '#',
		id: '6'
	}
]

export const UserMenuItems = [
	{
		id: '1',
		icon: 'icon-home',
		title: 'Main',
		url: '#'
	},
	{
		id: '8',
		icon: 'icon-exit',
		title: 'Exit',
		url: '#'
	}
]

export const noAuthMenuItems = [
	{
		id: '1',
		icon: 'icon-home',
		title: 'Main',
		url: '#'
	},
	{
		id: '2',
		icon: 'icon-categories',
		title: 'Categories',
		url: '#',
	},
	{
		id: '9',
		icon: 'icon-exit',
		title: 'Sign In',
		url: '#'
	}
]
